
"""
Exercise: Find at least five bugs

Description:
    The goal of this code is to create summary statistics for the monthly returns of stocks: APPL, AMZN, GOOG and TSLA.
    Outputs: 
        - Average returns
        - Volatility of returns 
        - Cumulative returns


"""

# %% Import Modules
import numpy as np
import pandas as pd

# %% Import Data 
mdata1 = pd.read_csv('data1.csv')
mdata2 = pd.read_csv('data2.csv')
mdata1.head()
mdata1.describe()
mdata1.Ticker
mdata2.Ticker

# %% Append data 
mdata = mdata1.append(mdata2)
mdata.reset_index(inplace=True)
mdata.columns
mdata.head()

# %% Fix ticker
## Wrong tickers
wrong_tickers = {'AAPL': ['AA PL', 'APPL']}
mdata.loc[mdata['Ticker'].isin(wrong_tickers['AAPL']), 'Ticker'] = 'AAPL'

# %% Calculate avegage returns
ret_mean = mdata.groupby('Ticker').Return.mean()
ret_mean

# %% Calculate return volatility
ret_mean = mdata.groupby('Ticker').Return.std()
ret_mean

# %% Calculate cumulative returns
mdata['RawReturn'] = mdata['Return']+1
ret_cum = mdata.groupby('Ticker').RawReturn.prod()
ret_cum

# %% Check with cumulative returns from the data
mdata['CumulativeReturn']
mdata.loc[mdata.Date == '08/18/20', ['Ticker', 'CumulativeReturn']]
ret_cum

# %% Final Table
results = pd.DataFrame({'Mean':ret_mean, 'Volatility': ret_vol, 'Cumulative': ret_cum})
results
