
"""
Exercise: Find seven bugs

Description:
    The goal of this code is to create summary statistics for the monthly returns of stocks: APPL, AMZN, GOOG and TSLA.
    Outputs: 
        - Average returns
        - Volatility of returns 
        - Cumulative returns


"""

# %% Import Modules
import numpy as np
import pandas as pd

# %% Import Data 
mdata1 = pd.read_csv('data1.csv')
mdata2 = pd.read_csv('data2.csv')


# %% Append data 
# Bug 1: Wrong colums name
mdata1.columns
mdata2.columns
mdata1.rename(columns = {'Returns': 'Return'}, inplace = True)


mdata = mdata1.append(mdata2)
mdata.reset_index(inplace=True)
mdata.columns
mdata.head()



# %% Fix ticker
## Wrong tickers

### BUG 2: Wrong ticker name for GOOG
wrong_tickers = {'AAPL': ['AA PL', 'APPL'],
                'GOOG': ['GOOG.']}

mdata.loc[mdata['Ticker'].isin(wrong_tickers['AAPL']), 'Ticker'] = 'AAPL'
mdata.loc[mdata['Ticker'].isin(wrong_tickers['GOOG']), 'Ticker'] = 'GOOG'

# %% Calculate avegage returns
ret_mean = mdata.groupby('Ticker').Return.mean()
ret_mean

# %% Calculate return volatility
## BUG 3: Wrong naming 
ret_vol = mdata.groupby('Ticker').Return.std()
ret_vol

# %% Calculate cumulative returns
## BUG 4: forget it is in %
mdata['RawReturn'] = (mdata['Return']/100)+1
ret_cum = mdata.groupby('Ticker').RawReturn.prod()*100

# %% Check with cumulative returns from the data
# BUG 5: wrong date
mdata['CumulativeReturn']
mdata.loc[mdata.Date == '8/18/20', ['Ticker', 'CumulativeReturn']]
ret_cum

# %% Final Table
results = pd.DataFrame({'Mean':ret_mean, 'Volatility': ret_vol, 'Cumulative': ret_cum})
results
