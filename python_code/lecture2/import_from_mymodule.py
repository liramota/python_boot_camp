#!/usr/bin/env python

"""
Import ask_the_weather from mymodule and use it

Author: Lira Mota, lmota20@gsb.columbia.edu
Date: August, 2019

"""

# %% Import Modules
from python_code.lecture2.mymodule import say_hi
from python_code.lecture2.mymodule import ask_the_weather

# %% Use function from mymodule
say_hi('Lira')
ask_the_weather('New York City')