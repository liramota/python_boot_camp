
"""
Import (full) and use mymodule

Author: Lira Mota, lmota20@gsb.columbia.edu
Date: August, 2020

"""

# %% Import Modules
import python_code.mymodule as my

# %% Use mymodule

# Say Hi
my.say_hi('Lira')
my.say_hi('Lira', ask_how_are_you=True)

# Ask the weather
my.ask_the_weather('New York City')
