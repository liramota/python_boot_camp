
"""
My first Module

Author: Lira Mota, lmota20@gsb.columbia.edu
Date: August, 2020
"""


def say_hi(x, ask_how_are_you=True):
    """
    A function to say hi.

    Parameters
    ----------
        x: character
        ask_how_are_you: boolean

    Examples
    --------
        x = say_hi(x='Lira', ask_how_are_you=False)
    """
    y = 'Hi ' + x + '!'
    if ask_how_are_you:
        y = y + ' How are you today?'

    return y


def ask_the_weather(x, period='today'):
    """
        A function to say hi.

        Parameters
        ----------
            x: character
            ask_how_are_you: boolean

        Examples
        --------
            x = say_hi(x='Lira', ask_how_are_you=False)
        """
    y = 'How is the weather %s in %s?' % (period, x)

    return y


