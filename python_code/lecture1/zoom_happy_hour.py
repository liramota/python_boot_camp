import numpy as np
people = ['Guido van Rossum',
          'Mickey Mouse',
          'Johnny Depp',
          'Costis Maglaras',
          'Scarlett Johansson',
          'that person you forgot to call yesterday',
          'Mari'
          ]

games = ['pocker',
         'codenames',
         'animal crossing',
         'some random game']

# %% Choose one game
game = np.random.choice(games)

# %% Choose one person 
person1 = np.random.choice(people)

print(f"How about a zoom meeting with {person1} to play {game}?")

