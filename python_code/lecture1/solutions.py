# Exercise 1
# https://www.hackerrank.com/challenges/python-loops/problem

if __name__ == '__main__':
	n = int(input())
	for k in range(n):
		print(k**2)


# Exercise 2
# https://www.hackerrank.com/challenges/py-if-else/problem

if __name__ == '__main__':
	n = int(input().strip())
	if n%2 == 1 or (n>=6 and n<=20):
		print('Weird')
	else:
		print('Not Weird')


# Exercise 3
# https://www.hackerrank.com/challenges/write-a-function/problem

def is_leap(year):
	'''
	Input
	year (Integer): year to be checked
	Output (Boolean): True if 'year' is leap year, False otherwise.
	'''
    if year%400==0:
        return True
    elif year%100==0:
        return False
    elif year%4==0:
        return True
    else:
        return False
    
    # Write your logic here
    
    return leap

year = int(input())
print(is_leap(year))

# Exercise 4
# https://www.hackerrank.com/challenges/text-wrap/problem

def text_wrap(mystring, wrap_int = 4):
    i = 0
    while i < len(mystring):
        j = min(i + wrap_int, len(mystring))
        print(mystring[i : j])
        i = i + wrap_int # i += wrap_int
        
# Exercise 5
# https://www.hackerrank.com/challenges/string-validators/problem

def test_string(mystring):
    print(any([c.isalnum() for c in mystring]))
    print(any([c.isalpha() for c in mystring]))
    print(any([c.isdigit() for c in mystring]))
    print(any([c.islower() for c in mystring]))
    print(any([c.isupper() for c in mystring]))

# What if the string is long, say millions of characters? Can you do better?
# print(any(map(str.isalnum,mystring)))
