"""
Exercise 1 - Python Boot Camp, Summer 2020

Author: Lira Mota, lmota20@gsb.columbia.edu
Date: August, 2020

"""

hw = 'Hello world!'
hw
# This is a comment. Lines that begin with the character `#` are ignored by the Python interpreter.
#print('Hello world!')

