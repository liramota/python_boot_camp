# README #

Hello!

Welcome to the GIT repository created for the Columbia Business School Python Boot Camp, Summer 2021.

### What is this repository for? ###

* Here you can find all lecture notes;
* You can find code used in class;
* You can also track any recent changes in the class material.

### Lectures References ###

These lectures and pieces of code were inspired by many great sources of Python knowledge. 

* Wes McKinney�s book: https://wesmckinney.com/pages/book.html
* QuantEcon: https://lectures.quantecon.org
* Kriste Krstovski's Computing for Business lecture notes, 2018 CBS PhD course
* Dan Mechanic's Computing for Business lecture notes, 2017 CBS PhD course
* Mattan Griffel, Introduction to Python, 2018 CBS MBA couse

### Who do I talk to? ###

* Lira Mota - <lmota20@gsb.columbia.edu>
* Neel Shah (TA) - <NShah22@gsb.columbia.edu>
