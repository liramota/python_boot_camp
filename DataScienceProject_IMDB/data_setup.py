#!/usr/bin/env python
""""
Program: data_setup.py
@author: Lira Mota, lmota20@gsb.columbia.edu
@data: 08-2020

Description:
    Read IMDB data, create movie catorgories and features.
Dependencies:
    IMDB-Movie-Data.csv
    Pre-downloaded data from  https://www.kaggle.com/PromptCloudHQ/imdb-data
Output:
    imdb_movie_data_processed.csv
"""

# %% Packages
import numpy as np
import pandas as pd
from itertools import chain

# %% Load Data
imdb = pd.read_csv('data/IMDB-Movie-Data.csv')
imdb.rename(columns = {'Runtime (Minutes)':'Runtime', 'Revenue (Millions)': 'Revenue'}, inplace=True)

# %% Create Movie Categories
imdb['Quality'] = pd.cut(imdb.Rating, [1, 6, 6.5, 7.5, 10],
                         labels=['bad', 'maybe', 'good', 'great'],
                         include_lowest=True)

# %% Create New Features
# Create genre dummies
genre_features = imdb['Genre'].str.get_dummies(sep=',')
imdb = imdb.join(genre_features)

# %% Save output
imdb.to_csv('data/imdb_processed.csv', index=False)